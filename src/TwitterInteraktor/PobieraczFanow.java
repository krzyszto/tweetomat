/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.User;

/**
 *
 * @author krzysztof
 */
public interface PobieraczFanow {
    PagableResponseList<User> pobierzFanow(String uzytkownik, Twitter sesja, long cursor) 
            throws TwitterNieUdaloSieException;
}
