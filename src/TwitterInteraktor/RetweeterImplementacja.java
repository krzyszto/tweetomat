/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 *
 * @author krzysztof
 */
public class RetweeterImplementacja implements Retweeter {
    protected RetweeterImplementacja() {}
    @Override
    public void retweetnij(Status tweet, Twitter sesja) throws TwitterNieUdaloSieException {
        try {
            sesja.retweetStatus(tweet.getId());
        } catch (TwitterException ex) {
            //znaczy, ze tweet zostal juz zretweetowany przez nas.
        }
    }
}
