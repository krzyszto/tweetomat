/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import java.util.Set;
import twitter4j.PagableResponseList;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.User;

/**
 *
 * @author krzysztof
 */
public class TwitterIOImplementacja implements TwitterIO {
    private final Logowacz logowacz;
    private final PobieraczFanow pobieraczFanow;
    private final Retweeter retweeter;
    private final PobieraczTweetow pobieraczTweetow;
    public TwitterIOImplementacja() {
        retweeter = new RetweeterImplementacja();
        pobieraczFanow = new PobieraczFanowImplementacja();
        logowacz = new LogowaczImplementacja();
        pobieraczTweetow = new PobieraczTweetowImplementacja();
    }
    
    @Override
    public void retweetnij(Set<String> konta) throws TwitterNieUdaloSieException {
        for (String konto : konta) {
            for (Status tweet : pobieraczTweetow.podajTweety(konto, logowacz.dajSesje())) {
                retweeter.retweetnij(tweet, logowacz.dajSesje());
            }
        }
    }

    @Override
    public ResponseList<Status> pobierzTweety(String uzytkownik) throws TwitterNieUdaloSieException {
        return pobieraczTweetow.podajTweety(uzytkownik, logowacz.dajSesje());
    }
    
    @Override
    public PagableResponseList<User> pobierzFanow(String uzytkownik, long cursor) throws TwitterNieUdaloSieException {
        return pobieraczFanow.pobierzFanow(uzytkownik, logowacz.dajSesje(), cursor);
    }

}
