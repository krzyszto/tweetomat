/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import java.util.Set;
import twitter4j.PagableResponseList;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.User;

/**
 *
 * @author krzysztof
 */
public interface TwitterIO {
    void retweetnij(Set<String> konta) throws TwitterNieUdaloSieException;
    PagableResponseList<User> pobierzFanow(String uzytkownik, long cursor) throws TwitterNieUdaloSieException;
    ResponseList<Status> pobierzTweety(String uzytkownik) throws TwitterNieUdaloSieException;
}
