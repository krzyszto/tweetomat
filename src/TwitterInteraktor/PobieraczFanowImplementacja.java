/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 *
 * @author krzysztof
 */
public class PobieraczFanowImplementacja implements PobieraczFanow {
    protected PobieraczFanowImplementacja() {}
    @Override
    public PagableResponseList<User> pobierzFanow(String uzytkownik, Twitter sesja, long cursor) 
            throws TwitterNieUdaloSieException {
        System.out.println("Pobieranie fanow "+uzytkownik+".");
        try {
            return sesja.getFollowersList(uzytkownik, cursor);
        } catch (TwitterException ex) {
            if (ex.exceededRateLimitation()) {
                System.err.println("Przekroczono dostepny transfer.");
            }
            return null;
        }
    }

    
}
