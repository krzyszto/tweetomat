/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 *
 * @author krzysztof
 */
public class PobieraczTweetowImplementacja implements PobieraczTweetow {
    protected PobieraczTweetowImplementacja() {}
    @Override
    public ResponseList<Status> podajTweety(String uzytkownik, Twitter sesja) 
            throws TwitterNieUdaloSieException {
        ResponseList<Status> wynik = null;
        System.out.println("Pobieramy tweety uzytkownika "+uzytkownik+".");
        try {
            return sesja.getUserTimeline(uzytkownik);
        } catch (TwitterException ex) {
            throw new TwitterNieUdaloSieException();
        }
    }
}
