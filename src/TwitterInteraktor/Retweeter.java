/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TwitterInteraktor;

import twitter4j.Status;
import twitter4j.Twitter;

/**
 *
 * @author krzysztof
 */
public interface Retweeter {
    void retweetnij(Status tweet, Twitter sesja) throws TwitterNieUdaloSieException;
}
