/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tweetomat;

import IO.WejscieWyjscie;
import IO.WejscieWyjscieImplementacja;
import SQL.CzarodziejSQLa;
import SQL.CzarodziejSQLaImplementacja;
import SQL.SQLNieByloException;
import SQL.SQLNieUdaloSieException;
import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterIOImplementacja;
import TwitterInteraktor.TwitterNieUdaloSieException;
import java.util.NoSuchElementException;
import java.util.Set;
/**
 *
 * @author krzysztof
 */
public class MistrzImplementacja implements Mistrz {
    private final WejscieWyjscie stdio;
    private final TwitterIO twitterio;
    private final CzarodziejSQLa sql;
    final int WYJSCIE_Z_PROGRAMU = 0;
    final int POMOC = 1;    
    final int WYZNACZ_NAJLEPSZYCH_LUDZI = 2;
    final int RETWEETUJ = 3;
    final int ZAKTUALIZUJ_BAZE_FANOW = 4;
    final int ZAKTUALIZUJ_BAZE_TWEETOW = 5;
    final int DODAJ_NOWEGO = 6;
    final int USUN_UZYTKOWNIKA = 7;
    final int WYPISZ_WSZYSTKICIH_DO_RETWEETOWANIA = 8;
    final int WYPISZ_WSZYSTKICH = 9;
    final String pomoc = "Aby wyjsc z programu wpisz " + WYJSCIE_Z_PROGRAMU + 
            ".\n" + "Aby wyswietlic ten komunikat wpisz " + POMOC + ".\n" +
            "Aby system wyznaczyl najpopularniejszych ludzi do retweetowania "
            + "wpisz " + 2 +".\n" +
            "Aby system retweetowal posty najpopularniejszych ludzi wpisz " +
            RETWEETUJ + ".\n" + 
            "Aby system zaktualizowal baze fanow wpisz " + 
            ZAKTUALIZUJ_BAZE_FANOW + ".\n" +
            "Aby system zaktualizowal baze tweetow wpisz " +
            ZAKTUALIZUJ_BAZE_TWEETOW + ".\n" +
            "Aby system dodal nowe konto wpisz " + DODAJ_NOWEGO + ".\n" + 
            "Aby system usunal konto wpisz " + USUN_UZYTKOWNIKA + ".\n" +
            "Aby zobaczyc, kto aktualnie jest uznawany za wartego " +
            "retweetowania wpisz " + WYPISZ_WSZYSTKICIH_DO_RETWEETOWANIA + ".\n" +
            "Aby zobaczyc wszystkich, ktorzy sa rozwazani w algorytmach wpisz " +
            WYPISZ_WSZYSTKICH + ".";
    public MistrzImplementacja() {
        stdio = new WejscieWyjscieImplementacja();
        twitterio = new TwitterIOImplementacja();
        sql = new CzarodziejSQLaImplementacja();
        stdio.wypisz(pomoc);
    }
    
    private void zakonczSie() {
        stdio.wypisz("Do zobaczenia.");
    }

    @Override
    public void dzialaj() {
        while (true) {
            int polecenie;
            try {
                polecenie = stdio.podajKolejnePolecenie();
            } catch (NoSuchElementException ex) {
                zakonczSie();
                return;
            }
            switch (polecenie) {
                case WYJSCIE_Z_PROGRAMU:
                    stdio.wypisz("Do zobaczenia.");
                    return;
                case POMOC:
                    stdio.wypisz(pomoc);
                    break;
                case WYZNACZ_NAJLEPSZYCH_LUDZI:
                    try {
                        stdio.wypisz("Wyznacze najlepszych ludzi do retweetowania.");
                        sql.wyznaczLudziDoRetweetowania();
                        stdio.wypisz("Ponownie wyznaczono najlepszych ludzi.");
                    } catch (SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie wyznaczyc najlepszych ludzi do retweetowania.");
                    }
                    break;
                case RETWEETUJ:
                    try {
                        stdio.wypisz("Zretweetuje najpopularniejszych uzytkownikow.");
                        twitterio.retweetnij(sql.dajNajlepszychLudziDoRetweetowania());
                        stdio.wypisz("Zretweetowano tweety popularnych ludzi.");
                    } catch (TwitterNieUdaloSieException | SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie zretweetowac.");
                    }
                    break;
                case ZAKTUALIZUJ_BAZE_FANOW:
                    stdio.wypisz("Zaktualizuje baze fanow.");
                    try {
                        sql.uaktualnijFanow(twitterio);
                        stdio.wypisz("Uaktualniono fanow.");
                    } catch (TwitterNieUdaloSieException | SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie uaktualnic fanow.");
                    }
                    break;
                case DODAJ_NOWEGO:
                    stdio.wypisz("Podaj nazwe uzytkownika, jakiego chcesz dodac do bazy.");
                    String tmp = stdio.podajSlowo();
                    try {
                        sql.dodajNowego(tmp);
                        stdio.wypisz("Dodano uzytkownika " + tmp +".");
                    } catch (SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie dodac uzytkownika do bazy.");
                    }
                    break;
                case ZAKTUALIZUJ_BAZE_TWEETOW:
                    stdio.wypisz("Zaktualizuje baze tweetow.");
                    try {
                        sql.zaktualizujBazeTweetow(twitterio);
                        stdio.wypisz("Zaktualizowalem baze tweetow.");
                    } catch (SQLNieUdaloSieException | TwitterNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie zaktualizowac bazy tweetow.");
                    }
                    break;
                case USUN_UZYTKOWNIKA:
                    stdio.wypisz("Podaj nazwe uzytkownika, "
                            + "jakiego chcesz usunac z bazy.");
                    String tmp2;
                    try {
                        tmp2 = stdio.podajSlowo();
                    } catch (NoSuchElementException ex) {
                        zakonczSie();
                        return;
                    }
                    try {
                        sql.usunUzytkownikaZBazy(tmp2);
                        stdio.wypisz("Usunieto uzytkownika " + tmp2 + ".");
                    } catch (SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie usunieto uzytkownika " + tmp2 + ".");
                    } catch (SQLNieByloException ex) {
                        stdio.wypisz("Uzytkownika " + tmp2 + " nie bylo w bazie.");
                    }
                    break;
                case WYPISZ_WSZYSTKICIH_DO_RETWEETOWANIA:
                    try {
                        Set<String> uzytkownicy = sql.dajNajlepszychLudziDoRetweetowania();
                        if (uzytkownicy.isEmpty())
                            throw new SQLNieByloException();
                        stdio.wypisz("Oto konta, ktore program uznaje "
                                + "za warte retweetowania:");
                        for (String uzytkownik : uzytkownicy) {
                            stdio.wypisz(uzytkownik);
                        }
                        stdio.wypisz("Program twierdzi, ze nie bedzie "
                                + "retweetowal innych kont, oprocz tych "
                                + "wypisanych powyzej.");
                    } catch (SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie pobrac uzytkownikow, ktorzy beda retweetowani");
                    } catch (SQLNieByloException ex) {
                        stdio.wypisz("System nie bedzie retweetowal "
                                + "zadnego uzytkownika, bo uznal, "
                                + "ze nie ma nikogo wartego, badz jeszcze nie wyznaczyl,");
                    }
                    break;
                case WYPISZ_WSZYSTKICH:
                    try {
                        Set<String> uzytkownicy = sql.dajWszystkichLudzi();
                        if (uzytkownicy.isEmpty())
                            throw new SQLNieByloException();
                        stdio.wypisz("Oto wszyscy uzytkownicy, ktorzy sa "
                                + "potencjalnymi kandydatami do retweetowania.");
                        for (String uzytkownik : uzytkownicy) {
                            stdio.wypisz(uzytkownik);
                        }
                        stdio.wypisz("To wszyscy uzytkownicy, jacy sa "
                                + "potencjalnymi kandydatami do retweetowania.");
                    } catch (SQLNieByloException ex) {
                        stdio.wypisz("Program nie ma zadnych uzytkownikow w bazie.");
                    } catch (SQLNieUdaloSieException ex) {
                        stdio.wypisz("Nie udalo sie pobrac uzytkownikow z bazy.");
                    }
                    break;
                default:
                    stdio.wypisz("Nie znam takiej instrukcji.");
                    break;
                }
            }
        }
    }
