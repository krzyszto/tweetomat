/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author krzysztof
 */
interface CzarodziejSesji {
    Statement dajSesje();
    PreparedStatement dajPreparedStatement(String format) throws SQLNieUdaloSieException;
}
