/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import java.sql.Statement;
import java.util.Set;

/**
 *
 * @author krzysztof
 */
public interface WyznaczatorNajlepszychLudzi {
    void wyznaczLudziDoRetweetowania(Statement sesja) throws SQLNieUdaloSieException;
    Set<String> dajLudziDoRetweetowania(Statement sesja) throws SQLNieUdaloSieException;
    Set<String> dajWszystkichLudzi(Statement sesja) throws SQLNieByloException, SQLNieUdaloSieException;
}
