/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author krzysztof
 */
public class WyznaczatorNajlepszychLudziImplementacja implements WyznaczatorNajlepszychLudzi{
    private final int GORNY_PROG_LICZBY_TWEETOW_DZIENNIE = 20;
    private final String LICZBA_TWEETOW_DZIENNIE_DLA_NIEPISZACYCH = "132534546356";
    protected WyznaczatorNajlepszychLudziImplementacja() {}
    private void obliczIleKtoryDziennieTweetow(Statement sesja) throws SQLNieUdaloSieException {
        try {
            sesja.execute("update konta "
                    + "set liczbatweetowdziennie = ("
                    + "select count(*) * 1.0 / "
                    + "(date(CURRENT_DATE) - min(date(data)) + 1) "
                    + "from tweet "
                    + "where tweet.konto = konta.konto group by konto)");
            sesja.execute("update konta "
                    + "set liczbatweetowdziennie = " 
                    + LICZBA_TWEETOW_DZIENNIE_DLA_NIEPISZACYCH
                    + "where liczbatweetowdziennie IS NULL");
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }

    private void nikogoNieRetweetujemy(Statement sesja) throws SQLNieUdaloSieException {
        try {
            sesja.execute("update konta set czyretweetowac = false");
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
    private float ileTweetowDziennieBedziemyDawac(Statement sesja) throws SQLNieUdaloSieException {
        try {
            ResultSet wynik = sesja.executeQuery("SELECT "
                    + "coalesce(sum(liczbatweetowdziennie), 0.0) AS "
                    + "dziennietweetow FROM konta "
                    + "WHERE czyretweetowac = true");
            if (wynik.next())
                return wynik.getFloat(1);
            return Float.MAX_VALUE;
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
    private int nieWybranych(Statement sesja) throws SQLNieUdaloSieException {
        try {
            ResultSet wynik = sesja.executeQuery(
                    "select count(*) from konta where czyretweetowac = false "
                            + "and liczbatweetowdziennie < "
                            + GORNY_PROG_LICZBY_TWEETOW_DZIENNIE);
            if (wynik.next())
                return wynik.getInt(1);
            return 0;
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
    @Override
    public void wyznaczLudziDoRetweetowania(Statement sesja) throws SQLNieUdaloSieException {
        nikogoNieRetweetujemy(sesja);
        obliczIleKtoryDziennieTweetow(sesja);
        while ((ileTweetowDziennieBedziemyDawac(sesja) < 5.1) && 
                (0 < nieWybranych(sesja))) {
            try {
                sesja.execute("update konta set czyretweetowac = true "
                        + "where konta.konto in "
                        + "(select konto from "
                        + "(select konto, count(*) as liczbafanow "
                        + "from followers "
                        + "where followers.konto "
                        + "not in (select konto "
                        + "from konta "
                        + "where czyretweetowac = true) "
                        + "group by konto) as fajnatabelka "
                        + "order by liczbafanow desc limit 1);");
            } catch (SQLException ex) {
                throw new SQLNieUdaloSieException();
            }
        }
    }

    @Override
    public Set<String> dajLudziDoRetweetowania(Statement sesja) throws SQLNieUdaloSieException {
        try {
            Set<String> wynik = new HashSet();
            ResultSet tmp = sesja.executeQuery(
                    "SELECT Konto FROM Konta WHERE CzyRetweetowac = true");
            while (tmp.next()) {
                wynik.add(tmp.getString("Konto"));
            }
            return wynik;
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
    @Override
    public Set<String> dajWszystkichLudzi(Statement sesja)
            throws SQLNieUdaloSieException {
        try {
            Set<String> wynik = new HashSet();
            ResultSet tmp = sesja.executeQuery("SELECT Konto FROM Konta");
            while (tmp.next()) {
                wynik.add(tmp.getString("Konto"));
            }
            return wynik;
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
}
