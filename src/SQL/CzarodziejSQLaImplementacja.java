/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;
import java.sql.SQLException;
import java.util.Set;

/**
 *
 * @author krzysztof
 */
public class CzarodziejSQLaImplementacja implements CzarodziejSQLa {
    private final AktualizatorBazyTweetow aktualizatorTweetow;
    private final WyznaczatorNajlepszychLudzi wyznaczator;
    private CzarodziejSesji sesyjny;
    private final AktualizatorBazyFanow aktualizatorBazyFanow;
    public CzarodziejSQLaImplementacja() {
        aktualizatorTweetow = new AktualizatorBazyTweetowImplementacja();
        wyznaczator = new WyznaczatorNajlepszychLudziImplementacja();
        aktualizatorBazyFanow = new AktualizatorBazyFanowImplementacja();
        try {
            sesyjny = new CzarodziejSesjiImplementacja();
        } catch (SQLException ex) {
            System.exit(-1);
        }
    }
    @Override
    public void dodajNowego(String nazwa_uzytkownika) 
            throws SQLNieUdaloSieException {
        aktualizatorTweetow.wlozUzytkownikaDoBazy(nazwa_uzytkownika, sesyjny);
    }

    @Override
    public void uaktualnijFanow(TwitterIO twiterIO) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException {
        aktualizatorBazyFanow.zaktualizujFanow(twiterIO, sesyjny);
    }

    @Override
    public void wyznaczLudziDoRetweetowania() 
            throws SQLNieUdaloSieException {
       wyznaczator.wyznaczLudziDoRetweetowania(sesyjny.dajSesje());
    }

    @Override
    public Set<String> dajNajlepszychLudziDoRetweetowania() 
            throws SQLNieUdaloSieException {
        return wyznaczator.dajLudziDoRetweetowania(sesyjny.dajSesje());
    }

    @Override
    public void zaktualizujBazeTweetow(TwitterIO twitterio) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException {
        aktualizatorTweetow.zaktualizujTweetyUzytkownikow(twitterio, sesyjny);
    }
    
    @Override
    public void usunUzytkownikaZBazy(String uzytkownik) 
            throws SQLNieUdaloSieException, SQLNieByloException {
        aktualizatorTweetow.usunUzytkownikaZBazy(uzytkownik, sesyjny);
    }
    
    @Override
    public Set<String> dajWszystkichLudzi() throws SQLNieByloException, 
            SQLNieUdaloSieException {
        return wyznaczator.dajWszystkichLudzi(sesyjny.dajSesje());
    }
}
