/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author krzysztof
 */
public class CzarodziejSesjiImplementacja implements CzarodziejSesji {
    private final Statement sesja;
    private final Connection polaczenie;
    protected CzarodziejSesjiImplementacja() throws SQLException {
        final String URL = "jdbc:postgresql://labdb/bd";
        polaczenie = DriverManager.getConnection(URL, "kp332534", "mama123");
        sesja = polaczenie.createStatement();
    }
    @Override
    public Statement dajSesje() {
        return sesja;
    }
    
    @Override
    public PreparedStatement dajPreparedStatement(String format) throws SQLNieUdaloSieException {
        try {
            return polaczenie.prepareStatement(format);
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
}
