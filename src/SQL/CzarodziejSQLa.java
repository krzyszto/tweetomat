/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;
import java.util.Set;

/**
 *
 * @author krzysztof
 */
public interface CzarodziejSQLa {
    void dodajNowego(String nazwa_uzytkownika) 
            throws SQLNieUdaloSieException;
    void uaktualnijFanow(TwitterIO twiterIO) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException;
    void wyznaczLudziDoRetweetowania() 
            throws SQLNieUdaloSieException;
    Set<String> dajNajlepszychLudziDoRetweetowania() 
            throws SQLNieUdaloSieException;
    void zaktualizujBazeTweetow(TwitterIO twitterio) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException;
    void usunUzytkownikaZBazy(String uzytkownik) 
            throws SQLNieUdaloSieException, SQLNieByloException;
    Set<String> dajWszystkichLudzi()
            throws SQLNieByloException, SQLNieUdaloSieException;
}
