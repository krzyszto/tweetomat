/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import twitter4j.ResponseList;
import twitter4j.Status;

/**
 *
 * @author krzysztof
 */
public class AktualizatorBazyTweetowImplementacja implements AktualizatorBazyTweetow {
    protected AktualizatorBazyTweetowImplementacja() {}
    @Override
    public void wlozTweety(ResponseList<Status> tweety, CzarodziejSesji czarodziej) throws SQLNieUdaloSieException {
        final String FORMAT = "INSERT INTO Tweet VALUES (?,?,?)";
        if (tweety == null) {
            throw new SQLNieUdaloSieException();
        }
        for (Status tweet : tweety) {
            try {
                PreparedStatement tmp = czarodziej.dajPreparedStatement(FORMAT);
                tmp.setString(1, tweet.getUser().getScreenName());
                tmp.setDate(2, new Date(tweet.getCreatedAt().getTime()));
                tmp.setLong(3, tweet.getId());
                tmp.execute();
            } catch (SQLException ex) {
            }
        }
    }
    
    @Override
    public void zaktualizujTweetyUzytkownikow(TwitterIO twitterio, 
            CzarodziejSesji czarodziej) throws SQLNieUdaloSieException, TwitterNieUdaloSieException {
        final String FORMAT = "SELECT Konto FROM Konta";
        try {
            ResultSet tmp = czarodziej.dajPreparedStatement(FORMAT)
                    .executeQuery();
            while (tmp.next()) {
                wlozTweety(twitterio.pobierzTweety(tmp.getString(1)), 
                        czarodziej);
            }
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }

    @Override
    public void wlozUzytkownikaDoBazy(String uzytkownik, 
            CzarodziejSesji czarodziej) throws SQLNieUdaloSieException {
        try {
            final String FORMAT = "INSERT INTO Konta VALUES (?,?,?)";
            PreparedStatement tmp = czarodziej.dajPreparedStatement(FORMAT);
            tmp.setString(1, uzytkownik);
            tmp.setBoolean(3, false);
            tmp.setFloat(2, Float.MAX_VALUE);
            tmp.execute();
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
    @Override
    public void usunUzytkownikaZBazy(String uzytkownik, CzarodziejSesji czarodziej) 
            throws SQLNieUdaloSieException, SQLNieByloException {
        try {
            final String FORMAT1 = "DELETE FROM ";
            final String FORMAT2 = " WHERE Konto = ?";
            final String[] tabele = {"followers", "tweet", "konta"};
            for (String ktora_tabela : tabele) {
                PreparedStatement tmp = czarodziej.dajPreparedStatement(
                        FORMAT1 + ktora_tabela + FORMAT2);
                tmp.setString(1, uzytkownik);
                if ((tmp.executeUpdate() > 0) && (ktora_tabela.equals("konta"))) {
                } else if (ktora_tabela.equals("konta")) {
                    throw new SQLNieByloException();
                }
            }
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
}
