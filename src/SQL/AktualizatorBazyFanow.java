/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;

/**
 *
 * @author krzysztof
 */
public interface AktualizatorBazyFanow {
    void zaktualizujFanow(TwitterIO twitterio, CzarodziejSesji czarodziej) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException;
}
