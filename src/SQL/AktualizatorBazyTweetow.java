/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;
import twitter4j.ResponseList;
import twitter4j.Status;
/**
 *
 * @author krzysztof
 */
public interface AktualizatorBazyTweetow {
    void wlozTweety(ResponseList<Status> tweety, CzarodziejSesji czarodziej) throws SQLNieUdaloSieException;
    void zaktualizujTweetyUzytkownikow(TwitterIO twitterio, 
            CzarodziejSesji czarodziej) throws SQLNieUdaloSieException, TwitterNieUdaloSieException;
    void wlozUzytkownikaDoBazy(String uzytkownik, CzarodziejSesji czarodziej) throws SQLNieUdaloSieException;
    void usunUzytkownikaZBazy(String uzytkownik, CzarodziejSesji czarodziej) throws SQLNieUdaloSieException, SQLNieByloException;
}
