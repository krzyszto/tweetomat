/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SQL;

import TwitterInteraktor.TwitterIO;
import TwitterInteraktor.TwitterNieUdaloSieException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import twitter4j.PagableResponseList;
import twitter4j.User;

/**
 *
 * @author krzysztof
 */
public class AktualizatorBazyFanowImplementacja implements AktualizatorBazyFanow {
    protected AktualizatorBazyFanowImplementacja() {}
    @Override
    public void zaktualizujFanow(TwitterIO twitterio, CzarodziejSesji czarodziej) 
            throws SQLNieUdaloSieException, TwitterNieUdaloSieException {
        try {
            final String FORMAT_ZAPYTANIA = "SELECT Konto from Konta";
            final String WRZUC_KROTKE = "INSERT into Followers VALUES (?,?)";
            ResultSet konta_do_retweetowania;
            konta_do_retweetowania =
                    czarodziej.dajPreparedStatement(FORMAT_ZAPYTANIA).executeQuery();
            while (konta_do_retweetowania.next()) {
                long cursor = -1;
                do {
                    if (cursor == 0)
                        break;
                    PagableResponseList<User> fanowie = 
                            twitterio.pobierzFanow(konta_do_retweetowania.getString(1), cursor);
                    if (fanowie == null) {
                        cursor = 0;
                        continue;
                    }
                    for (User u : fanowie) {
                        PreparedStatement insercja = czarodziej.dajPreparedStatement(WRZUC_KROTKE);
                        insercja.setString(1, konta_do_retweetowania.getString(1));
                        insercja.setString(2, u.getScreenName());
                        try {
                            insercja.execute();
                        } catch (SQLException ex) {
                        }
                    }
                    cursor = fanowie.getNextCursor();
                } while (cursor != 0);
            }
        } catch (SQLException ex) {
            throw new SQLNieUdaloSieException();
        }
    }
    
}
