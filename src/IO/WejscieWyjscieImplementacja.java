/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package IO;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author krzysztof
 */
public class WejscieWyjscieImplementacja implements WejscieWyjscie {
    private final Scanner stdin;
    public WejscieWyjscieImplementacja() {
        stdin = new Scanner(System.in);
    }
    @Override
    public void wypisz(String komunikat) {
        System.out.println(komunikat);
    }
    @Override
    public int podajKolejnePolecenie() throws NoSuchElementException {
        while (true) {
            if (!stdin.hasNextInt()) {
                stdin.next();
                continue;
            }
//            try {
                return stdin.nextInt();
//            } catch (InputMismatchException ex) {
//            }
        }
    }
    @Override
    public String podajSlowo() {
        return stdin.next();
    }
}
