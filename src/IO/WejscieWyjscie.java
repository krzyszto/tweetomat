/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package IO;

import java.util.NoSuchElementException;

/**
 *
 * @author krzysztof
 */
public interface WejscieWyjscie {
    int podajKolejnePolecenie() throws NoSuchElementException;
    void wypisz(String komunikat);
    String podajSlowo();
}
